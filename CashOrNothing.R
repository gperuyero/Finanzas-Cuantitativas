#OPCIONES BINARIAS CASH OR NOTHING

#Para llamar estas funciones, hay que darle los valores del Precio del Activo Subyacente, 
#Precio Strike, Tasa Libre de Riesgo Anual, Volatilidad Estimada por Maxima Versomilitud 
#utilizando Rendimientos Logaritmicos, Tiempo Final y Tiempo en el cual se quiere 
#estimar el valor de la opcion, los dos ultimos datos en dias. 

#CALL
FuncionCall_CRN <- function(S, K, r, sigma, TT, Tt)
{
  TT <- TT/252
  Tt <- Tt/252
  r <- r/100/252
  
  d1 <- (log(S/K) + (r + (sigma^2)/2)*(TT - Tt)) / sigma*sqrt(TT - Tt)
  d2 <- d1 - sigma*sqrt(TT - Tt)
  
  ValorCall <- exp(-r*(TT - Tt))*pnorm(d2)
  ValorCall
}

#PUT
FuncionPut_CRN <- function(S, K, r, sigma, TT, Tt)
{
  TT <- TT/252
  Tt <- Tt/252
  r <- r/100/252
  
  d1 <- (log(S/K) + (r + (sigma^2)/2)*(TT - Tt)) / sigma*sqrt(TT - Tt)
  d2 <- d1 - sigma*sqrt(TT - Tt)
  
  ValorPut <- exp(-r*(TT - Tt))*pnorm(-d2)
  ValorPut
}